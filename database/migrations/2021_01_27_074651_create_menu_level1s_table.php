<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuLevel1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_level1s', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('menu_name')->nullable();
            $table->string('menu_class')->nullable();
            $table->string('menu_url')->nullable();
            $table->integer('have_submenu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_level1s');
    }
}
