<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(MenuLevel1Seeder::class);
        $this->call(MenuLevel2Seeder::class);
        // \App\Models\User::factory(10)->create();
    }
}
