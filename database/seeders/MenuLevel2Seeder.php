<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu2;

class MenuLevel2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menulevel2_1 = new Menu2();
        $menulevel2_1->submenu_name = 'Category';
        $menulevel2_1->submenu_url = 'category.index';
        $menulevel2_1->menu_id = 1;
        $menulevel2_1->save();
        
        $menulevel2_3 = new Menu2();
        $menulevel2_3->submenu_name = 'All';
        $menulevel2_3->submenu_url = 'news.all';
        $menulevel2_3->menu_id = 2;
        $menulevel2_3->save();
        
        $menulevel2_4 = new Menu2();
        $menulevel2_4->submenu_name = 'Published';
        $menulevel2_4->submenu_url = 'news.published';
        $menulevel2_4->menu_id = 2;
        $menulevel2_4->save();
        
        $menulevel2_5 = new Menu2();
        $menulevel2_5->submenu_name = 'Pending';
        $menulevel2_5->submenu_url = 'news.pending';
        $menulevel2_5->menu_id = 2;
        $menulevel2_5->save();
        
        $menulevel2_6 = new Menu2();
        $menulevel2_6->submenu_name = 'Draft';
        $menulevel2_6->submenu_url = 'news.draft';
        $menulevel2_6->menu_id = 2;
        $menulevel2_6->save();
        
        $menulevel2_7 = new Menu2();
        $menulevel2_7->submenu_name = 'Menu Access';
        $menulevel2_7->submenu_url = 'utility.menu_access';
        $menulevel2_7->menu_id = 3;
        $menulevel2_7->save();
        
        $menulevel2_8 = new Menu2();
        $menulevel2_8->submenu_name = 'User Management';
        $menulevel2_8->submenu_url = 'utility.user_manage';
        $menulevel2_8->menu_id = 3;
        $menulevel2_8->save();
        
        $menulevel2_9 = new Menu2();
        $menulevel2_9->submenu_name = 'Menu Management';
        $menulevel2_9->submenu_url = 'utility.menu_manage';
        $menulevel2_9->menu_id = 3;
        $menulevel2_9->save();
        
        $menulevel2_10 = new Menu2();
        $menulevel2_10->submenu_name = 'User Group';
        $menulevel2_10->submenu_url = 'utility.user_group';
        $menulevel2_10->menu_id = 3;
        $menulevel2_10->save();
        
        $menulevel2_11 = new Menu2();
        $menulevel2_11->submenu_name = 'Change Password';
        $menulevel2_11->submenu_url = 'utility.change_pass';
        $menulevel2_11->menu_id = 3;
        $menulevel2_11->save();
        
        $menulevel2_12 = new Menu2();
        $menulevel2_12->submenu_name = 'Setting';
        $menulevel2_12->submenu_url = 'utility.setting';
        $menulevel2_12->menu_id = 3;
        $menulevel2_12->save();
    }
}
