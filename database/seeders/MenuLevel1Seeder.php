<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu1;

class MenuLevel1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menulevel1_1 = new Menu1();
        $menulevel1_1->menu_name = 'Master';
        $menulevel1_1->menu_class = 'fas fa-columns';
        $menulevel1_1->have_submenu = 1;
        $menulevel1_1->save();

        $menulevel1_2 = new Menu1();
        $menulevel1_2->menu_name = 'News';
        $menulevel1_2->menu_class = 'far fa-file-alt';
        $menulevel1_2->have_submenu = 1;
        $menulevel1_2->save();

        $menulevel1_3 = new Menu1();
        $menulevel1_3->menu_name = 'Utility';
        $menulevel1_3->menu_class = 'fas fa-cogs';
        $menulevel1_3->have_submenu = 1;
        $menulevel1_3->save();
    }
}
