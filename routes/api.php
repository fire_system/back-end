<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\RegisterController;
use API\NewsApiController;
use API\CategoryApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [RegisterController::class, 'login']);

Route::middleware('auth:api')->group(function () {
  Route::get('newsapi', [\App\Http\Controllers\API\NewsApiController::class, 'index'])->name('newsapi.index');
	Route::get('newsapi/{newsapi:slug}', [\App\Http\Controllers\API\NewsApiController::class, 'show'])->name('newsapi.show');
	Route::get('newscategory/{category:slug}', [\App\Http\Controllers\API\NewsApiController::class, 'showCatNews'])->name('newsapi.category.show');

	Route::resource('categoryapi', CategoryApiController::class)->scoped([
	  'categoryapi' => 'slug',
	]);
});
