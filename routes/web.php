<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['verify' => true]);

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::middleware('auth', 'verified')->group(function(){
	Route::get('master/category', 'CategoryController@index')->name('master.category');

	Route::resource('category', 'CategoryController')->scoped([
		'category' => 'slug',
	]);

	Route::get('news/all','NewsController@all')->name('news.all');
	Route::get('news/draft','NewsController@draft')->name('news.draft');
	Route::get('news/pending','NewsController@pending')->name('news.pending');
	Route::get('news/published','NewsController@published')->name('news.published');

	Route::patch('news/{news:slug}/edit', 'NewsController@update');
	Route::resource('news', 'NewsController')->scoped([
	    'news' => 'slug',
	]);

	Route::get('utility/menu', 'UtilityController@menu')->name('utility.menu_manage');

	Route::post('utility/savemenu1', 'UtilityController@savemenu1')->name('utility.saveMn1');
	Route::get('utility/editmenu1/{id}', 'UtilityController@editmenu1')->name('utility.editMn1');
	Route::post('utility/updatemenu1/{id}', 'UtilityController@updatemenu1')->name('utility.updateMn1');
	Route::get('utility/deletemenu1/{id}', 'UtilityController@deletemenu1')->name('utility.deleteMn1');

	Route::post('utility/savemenu2', 'UtilityController@savemenu2')->name('utility.saveMn2');
	Route::get('utility/editmenu2/{id}', 'UtilityController@editmenu2')->name('utility.editMn2');
	Route::post('utility/updatemenu2/{id}', 'UtilityController@updatemenu2')->name('utility.updateMn2');
	Route::get('utility/deletemenu2/{id}', 'UtilityController@deletemenu2')->name('utility.deleteMn2');

	Route::get('change_pass', 'ChangePasswordController@index')->name('change_pass');
	Route::post('change_pass', 'ChangePasswordController@store')->name('change.password');

	Route::get('assets/clients', 'AssetController@clients')->name('assets.clients');

	Route::get('assets/gallery', 'AssetController@gallery')->name('assets.gallery');
	Route::get('assets/gallery/upload', 'AssetController@newGallery')->name('assets.gallery.upload');
	Route::post('assets/gallery/store', 'AssetController@uploadGallery')->name('assets.gallery.store');
	Route::post('assets/gallery/destroy', 'AssetController@destroyGallery')->name('assets.gallery.destroy');
});
