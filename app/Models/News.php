<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = ['title','slug','thumbnail','thumbnail_temp','content','categori_id','author','status','tags'];

    function category()
    {
    	return $this->belongsTo(Category::class, 'categori_id');
    }

}
