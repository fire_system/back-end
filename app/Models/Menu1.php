<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Menu1 extends Model
{
    protected $table = 'menu_level1s';

    protected $fillable = [
        'id','menu_name', 'menu_class', 'menu_url', 'have_submenu'
    ];
}
