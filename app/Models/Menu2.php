<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Menu2 extends Model
{
    protected $table = 'menu_level2s';

    protected $fillable = [
        'id','submenu_name', 'submenu_url', 'menu_id'
    ];
}
