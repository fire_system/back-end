<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Videos extends Eloquent
{
    protected $collection = 'videos';

    protected $fillable = ['title','slug','video_id','thumbnail','temp_thumbnail','description','categori_id','author','status','tags'];
}
