<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    protected $fillable = [
        'name', 'slug'
    ];

    function news()
    {
    	return $this->hasMany(News::class);
    }
}
