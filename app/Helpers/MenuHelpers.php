<?php

namespace App\Helpers;

use App\Models\Menu1;
use App\Models\Menu2;
use App\Models\News;
use Illuminate\Support\Facades\Auth;

class MenuHelpers
{
    public static function menu_level1()
    {
        if (Auth::user()->email == 'buhranalmajeda@gmail.com') {
            $menu1 = Menu1::all();
        } else {
            $menu1 = Menu1::whereNotIn('menu_name',['Utility'])->get();
        }

    	return $menu1;
    }

    public static function menu_level2()
    {
    	$menu2 = Menu2::all();

    	return $menu2;
    }

    public static function countPost($id)
    {
        if ($id == 'all') {
            $valCount = News::count();
        } else {
            $valCount = News::where('status', $id)->count();
        }
        
        return $valCount;
    }
}
