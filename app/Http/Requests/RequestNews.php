<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestNews extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txttitle'     => 'required',
            'txtkategori'  => 'required',
            'txtkonten'    => 'required',
            'txttags'      => 'required',
            'txtauthor'    => 'required',
            'txtstatus'    => 'required'
        ];
    }
}
