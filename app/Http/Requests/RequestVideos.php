<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestVideos extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txttitle' => 'required|min:3|max:65',
            'txtthumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'txtvideoid' => 'required',
            'txtkategori' => 'required',
            'txtdeskripsi' => 'required',
            'txttags' => 'required',
            'txtauthor' => 'required',
            'txtstatus' => 'required'
        ];
    }
}
