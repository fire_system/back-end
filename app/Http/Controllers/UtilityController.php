<?php

namespace App\Http\Controllers;

use App\Models\Menu1;
use App\Models\Menu2;
use App\Http\Requests\RequestMenu1;
use App\Http\Requests\RequestMenu2;

class UtilityController extends Controller
{
    public function menu()
    {
        $menu1 = Menu1::get();
        $menu2 = Menu2::orderBy('menu_id','ASC')->get();
        return view('pages.utility.menu.index', compact('menu1','menu2'));
    }
    
    public function savemenu1(RequestMenu1 $request)
    {
        $menu1 = $request->all();
        $menu1['menu_name'] = request('txtMenuName');
        $menu1['menu_class'] = request('txtMenuClass');
        $menu1['menu_url'] = request('txtMenuUrl');
        $menu1['have_submenu'] = request('txtHaveSubMenu');

        Menu1::create($menu1);

        return redirect('utility/menu')->with('success','Menu Level 1 berhasil ditambah..!');
    }

    public function editmenu1($id)
    {
        $menu1 = Menu1::find($id);
        return view('pages.utility.menu.menu1.edit', compact('menu1'));
    }

    public function updatemenu1(RequestMenu1 $request, $id)
    {
        $menu1 = Menu1::find($id);
        $menu1['menu_name'] = request('txtMenuName');
        $menu1['menu_class'] = request('txtMenuClass');
        $menu1['menu_url'] = request('txtMenuUrl');
        $menu1['have_submenu'] = request('txtHaveSubMenu');

        $menu1->update();

        return redirect('utility/menu')->with('success','Menu Level 1 berhasil diedit..!');
    }

    public function deletemenu1($id)
    {
        $menu1 = Menu1::find($id);
        $menu1->delete();
        return redirect('utility/menu')->with('success','Menu Level 1 berhasil dihapus..!');
    }

    public function savemenu2(RequestMenu2 $request)
    {
        $menu2 = $request->all();
        $menu2['menu_id'] = request('txtmainmenu');
        $menu2['submenu_name'] = request('txtSubMenu');
        $menu2['submenu_url'] = request('txtLink');

        Menu2::create($menu2);

        return redirect('utility/menu')->with('success','Menu Level 2 berhasil ditambah..!');
    }

    public function editmenu2($id)
    {
        $menu1 = Menu1::get();
        $menu2 = Menu2::find($id);
        return view('pages.utility.menu.menu2.edit', compact('menu1','menu2'));
    }

    public function updatemenu2(RequestMenu2 $request, $id)
    {
        $menu2 = Menu2::find($id);
        $menu2['menu_id'] = request('txtmainmenu');
        $menu2['submenu_name'] = request('txtSubMenu');
        $menu2['submenu_url'] = request('txtLink');

        $menu2->update();

        return redirect('utility/menu')->with('success','Menu Level 2 berhasil diedit..!');
    }

    public function deletemenu2($id)
    {
        $menu2 = Menu2::find($id);
        $menu2->delete();
        return redirect('utility/menu')->with('success','Menu Level 2 berhasil dihapus..!');
    }
}
