<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use Illuminate\Http\Request;
use Image;

class AssetController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function clients()
    {
        return view('pages.clients.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gallery()
    {
        $dataimage = Asset::where('flag', 'gallery')->orderBy('created_at','desc')->get();
        return view('pages.gallery.index', compact('dataimage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newGallery()
    {
        return view('pages.gallery.upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadGallery(Request $request)
    {
        $images = $request->file('file');
        for ($i = 0; $i < count($images); $i++) {
            $asset = new Asset();
            $asset['image'] = $images[$i]->getClientOriginalName();
            $img = Image::make($images[$i]->path());

            $galleryMaxPath = storage_path('app/public/gallery/maximum');
            $img->resize(1280, 720, function ($constraint) {
                $constraint->aspectRatio();
            })->save($galleryMaxPath.'/'.$asset['image']);

            $galleryMinPath = storage_path('app/public/gallery/minimum');
            $img->resize(350, 322, function ($constraint) {
                $constraint->aspectRatio();
            })->save($galleryMinPath.'/'.$asset['image']);

            $destinationPath = storage_path('app/public/gallery');
            $images[$i]->move($destinationPath, $asset['image']);

            $asset['title'] = 'gallery-'.time();
            $asset['flag'] = 'gallery';

            $asset->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroyGallery(Request $request)
    {
        $dtasset = Asset::where('title', $request->title)->select('image','id')->first();
        if (file_exists(storage_path('app/public/gallery/'.$dtasset->image))) {
            unlink(storage_path('app/public/gallery/'.$dtasset->image));
            unlink(storage_path('app/public/gallery/maximum/'.$dtasset->image));
            unlink(storage_path('app/public/gallery/minimum/'.$dtasset->image));

            $getData = Asset::find($dtasset->id);

            if ($getData->delete()) {
                return true;
            } else {
                return false;
            }
        }
    }
}
