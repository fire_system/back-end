<?php

namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Category;
use Validator;
use App\Http\Resources\Category as CategoryResource;

/**
 * summary
 */
class CategoryApiController extends BaseController
{
    /**
     * summary
     */
    public function index()
    {
        $category = Category::get();
        return $this->sendResponse(new CategoryResource($category), 'category-api retrieved successfully.');
    }
}