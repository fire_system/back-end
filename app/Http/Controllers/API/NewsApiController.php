<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\News;
use App\Models\Category;
use Validator;
use App\Http\Resources\News as NewsResource;

/**
 * summary
 */
class NewsApiController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::with('category')->where('status', '1')->orderBy('created_at', 'desc')->paginate(4);
    
        return $this->sendResponse(new NewsResource($news), 'news-api retrieved successfully.');
    }

    public function show($newsapi)
    {
        $dtnews = News::join('category', 'news.categori_id', '=', 'category.id')->where('news.slug', $newsapi)->get(['news.*','category.name as category_name', 'category.slug as category_slug']);
        return $this->sendResponse($dtnews, 'news-api show data successfully.');
    }

    public function showCatNews(Category $category)
    {
        $newsCat = News::join('category', 'news.categori_id', '=', 'category.id')->where('category.slug', $category->slug)->orderBy('news.created_at', 'desc')->paginate(4, ['news.*','category.name as category_name', 'category.slug as category_slug']);
        return $this->sendResponse(new NewsResource($newsCat), 'news-api retrieved successfully.');
    }
}