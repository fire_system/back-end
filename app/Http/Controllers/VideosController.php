<?php

namespace App\Http\Controllers;

use App\Models\Videos;
use App\Http\Requests\RequestVideos;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = array(
            (object) array('id' => 1, 'name' => 'Publish'),
            (object) array('id' => 2, 'name' => 'Draft'),
            (object) array('id' => 3, 'name' => 'Pending')
        );
        return view('pages.videos.create',[
            'videos' => new Videos,
            'status' => $status,
            'categories' => Category::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestVideos $request)
    {
        // $extention = $request->file('txtthumb')->extention();
        // $imgName = Str::slug(request('txttitle')).'.'.$extention;
        // $path = Storage::putFileAs('public/thumbVideo', $request->file('txtthumb'), $imgName);

        $attr = new attr();
        $attr['title'] = request('txttitle');
        $attr['slug'] = Str::slug(request('txttitle'));
        $attr['thumbnail'] = request('txttitle');
        $attr['video_id'] = request('txtvideoid');
        $attr['content'] = request('txtkonten');
        $attr['categori_id'] = request('txtkategori');
        $attr['author'] = request('txtauthor');
        $attr['status'] = request('txtstatus');
        $attr['tags'] = request('txttags');

        // $attr->save();

        // return redirect('video/all')->with('success', 'Videos was Uploadted..!');

        return response()->json($attr);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function all()
    {
        $title = 'All';
        $video = Videos::get();
        $categori = Category::get();
        return view('pages.videos.list', compact('title','video','categori'));
    }

    public function draft()
    {
        $title = 'Draft';
        $video = Videos::where('status','1')->get();
        $categori = Category::get();
        return view('pages.videos.list', compact('title','video','categori'));
    }

    public function pending()
    {
        $title = 'Pending';
        $video = Videos::where('status','2')->get();
        $categori = Category::get();
        return view('pages.videos.list', compact('title','video','categori'));
    }

    public function published()
    {
        $title = 'Published';
        $video = Videos::where('status','3')->get();
        $categori = Category::get();
        return view('pages.videos.list', compact('title','video','categori'));
    }
}
