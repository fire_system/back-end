<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Http\Requests\RequestNews;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.news');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = array(
            (object) array('id' => 1, 'name' => 'Publish'),
            (object) array('id' => 2, 'name' => 'Draft'),
            (object) array('id' => 3, 'name' => 'Pending')
        );
        return view('pages.news.create', [
            'news' => new News,
            'categories' => Category::get(),
            'status' => $status
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestNews $request)
    {
        $news = new News();
        $news['title'] = $request->txttitle;
        $news['slug'] = Str::slug($request->txttitle);

        $file = $request->file('txtthumbnail');
        $imageName = 'news-'.time().uniqid().'.'.$file->getClientOriginalExtension();
        $file->move(storage_path('app/public/thumbnail'), $imageName);

        $news['thumbnail'] = $imageName;
        $news['thumbnail_temp'] = base64_encode($request->file('txtthumbnail'));
        $news['content'] = $request->txtkonten;
        $news['categori_id'] = $request->txtkategori;
        $news['author'] = $request->txtauthor;
        $news['status'] = $request->txtstatus;
        $news['tags'] = $request->txttags;
        
        $news->save();
        
        return redirect('news/all')->with('success', 'News was Created..!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        $category = Category::get();
        return view('pages.news.show',compact('news','category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        $attr = News::get();
        $categories = Category::get();
        $status = array(
            (object) array('id' => 1, 'name' => 'Publish'),
            (object) array('id' => 2, 'name' => 'Draft'),
            (object) array('id' => 3, 'name' => 'Pending')
        );

        return view('pages.news.edit', compact('news','categories','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestNews $request, News $news)
    {
        $attr = $request->all();
        $attr['title'] = $request->txttitle;
        $attr['slug'] = Str::slug($request->txttitle);

        if ($request->hasFile('txtthumbnail')) {
        	$request->validate([
        		'txtthumbnail' => 'required|image|mimes:jpeg,jpg,png|max:2048'
        	]);

            $file = $request->file('txtthumbnail');
            if(file_exists(public_path('storage/thumbnail/') . $news->thumbnail)){
                unlink(public_path('storage/thumbnail/') . $news->thumbnail);
            }

            $imageName = 'news-'.time().uniqid().'.'.$file->getClientOriginalExtension();
            $request->txtthumbnail->move(public_path('storage/thumbnail'), $imageName);

        }else{
            $imageName = $news->thumbnail;
        }

        $news['thumbnail'] = $imageName;
        $attr['content'] = $request->txtkonten;
        $attr['categori_id'] = $request->txtkategori;
        $attr['author'] = $request->txtauthor;
        $attr['status'] = $request->txtstatus;
        $attr['tags'] = $request->txttags;
        
        $news->update($attr);
        
        return redirect('news/all')->with('success', 'The News Was Updated..!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->delete();
        return redirect('news/all')->with('success', 'The News was Destroyed..!');
    }

    public function all()
    {
        $title = "All";
        $news = News::get();
        $category = Category::get();
        return view('pages.news.list',compact('news', 'category', 'title'));
    }

    public function published()
    {
        $title = "Published";
        $news = News::where('status','1')->get();
        $category = Category::get();
        return view('pages.news.list',compact('news', 'category', 'title'));
    }

    public function draft()
    {
        $title = "Draft";
        $news = News::where('status','2')->get();
        $category = Category::get();
        return view('pages.news.list',compact('news', 'category', 'title'));
    }

    public function pending()
    {
        $title = "Pending";
        $news = News::where('status','3')->get();
        $category = Category::get();
        return view('pages.news.list',compact('news', 'category', 'title'));
    }
}
