@extends('layouts.master')
@section('title', 'Our Client')
@section('content')
	<section class="section">
		<div class="section-header">
			<h1>Our Client</h1>
			<div class="section-header-breadcrumb">
				<div class="breadcrumb-item active"><a href="{{ route('assets.clients') }}">Our Client</a></div>
			</div>
		</div>
		<div class="section-body">
			@if(session('success'))
	      <div class="box-body">
	          <div class="alert alert-success alert-dismissible">
	              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	              <p style="margin-bottom: 0;"><i class="icon fa fa-check"></i></i> {{ session('success') }}</p>
	          </div>
	      </div>
      @endif
      @if (count($errors) > 0)
	      <div class="box-body">
	          <div class="alert alert-danger alert-dismissible">
	              <ul>
	                  @foreach ($errors->all() as $error)
	                      <li>{{ $error }}</li>
	                  @endforeach
	              </ul>
	          </div>
	      </div>
      @endif
			<div class="row">
				<div class="col-12 col-md-6 col-lg-12">
					<div class="card">
						<div class="card-body">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection