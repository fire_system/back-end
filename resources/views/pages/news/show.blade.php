@extends('layouts.master')
@section('title', 'Detail Post')
@section('content')
	<section class="section">
		<div class="section-header">
			<h1>Detail Post</h1>
			<div class="section-header-breadcrumb">
				<div class="breadcrumb-item active"><a href="/posts">Post</a></div>
	            <div class="breadcrumb-item">Detail Post</div>
			</div>
		</div>
		<div class="section-body">
			<h2 class="section-title">{{ $news->title }}</h2>
			<p class="section-lead">
                @foreach($category as $ctg)
                @if( $ctg->id === $news->categori_id )
                    {{ $ctg->name }}
                @endif
                @endforeach
            </p>
			<div class="row">
				<div class="col-12 col-md-6 col-lg-12">
					<div class="card">
						<div class="card-body">
							<img class="img-fluid" src={{asset('storage/thumbnail/'.$news->thumbnail)}}>
							<p class="card-text">
								{!! $news->content !!}
							</p>
							<div>
								Created at {{ $news->created_at->format('d F Y') }}
							</div>
						</div>
						<div class="card-footer d-flex justify-content-between">
              <form action="{{ route('news.destroy', $news->slug) }}" method="post" id="frm">
              	@method('delete')
              	@csrf
              	<input type="hidden" name="txtthumbnail" value="{{asset('storage/thumbnail/'.$news->thumbnail)}}">
                <button type="button" class="btn btn-link text-danger btn-sm" id="btndelete"><i class="fas fa-trash-alt"></i> Delete</button>
              </form>
							<a href="{{ URL::previous() }}" class="btn btn-md btn-info"><i class="fas fa-arrow-left"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('page-scripts')
	<script>
		"use strict";

		$('#btndelete').click(function(){
			swal({
                title: 'Are you sure delete this post?',
                text: '{{ $news->title }}',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
		    })
		    .then((willDelete) => {
                if (willDelete) {
                    $('#frm').submit();
		        } else {
                    swal('Your Post is safe!');
		        }
		    });
		});
	</script>
@endpush