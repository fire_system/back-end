@extends('layouts.master')
@section('title','News')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>News</h1>
        <div class="section-header-breadcrumb">
            <a href="{{ route('news.create') }}" class="btn btn-md btn-info" id="modal_menu_level_1"><i class="fas fa-plus"></i> Add News</a>
        </div>
    </div>
    <div class="section-body">
        @if(session('success'))
        <div class="box-body">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p style="margin-bottom: 0;"><i class="icon fa fa-check"></i></i> {{ session('success') }}</p>
            </div>
        </div>
        @endif
        @if (count($errors) > 0)
        <div class="box-body">
            <div class="alert alert-danger alert-dismissible">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-12 col-md-6 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>{{ $title }} News</h4>
                    </div>
                    <div class="card-body">
    					<div class="table-responsive">
    						<table id="list_post" class="table table-striped">
    							<thead>
    								<tr>
    									<th class="text-center pt-2">No</th>
    									<th>Title</th>
    									<th>Category</th>
    									<th>Tags</th>
    									<th>Author</th>
    									<th>Created</th>
    									<th>Status</th>
    								</tr>
    							</thead>
                                <tbody>
                                    @php $no = 1; @endphp
                                    @foreach($news as $row)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>
                                            {{ $row->title }}
                                            <div class="table-links">
                                                <a href="{{ route('news.show', ['news' => $row->slug]) }}" class="text-info" title="View Article"><i class="fas fa-eye"></i></a>
                                                <div class="bullet"></div>
                                                <a href="/news/{{ $row->slug }}/edit" class="text-primary" title="Edit Article"><i class="fas fa-pencil-alt"></i></a>
                                            </div>
                                        </td>
                                        <td>
                                            @foreach($category as $ctg)
                                            @if( $ctg->id === $row->categori_id )
                                                {{ $ctg->name }}
                                            @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @if ($row->tags !== '')
                                            @foreach (explode(',', $row->tags) as $tag)
                                            <span class="badge badge-primary m-1">{{ $tag }}</span>
                                            @endforeach
                                            @endif
                                        </td>
                                        <td>{{ strtoupper($row->author) }}</td>
                                        <td>{{ $row->created_at->diffForHumans() }}</td>
                                        <td>
                                            @if($row->status == '1')
                                                <div class="badge badge-primary">Published</div>
                                            @elseif($row->status == '2')
                                                <div class="badge badge-danger">Draft</div>
                                            @elseif($row->status == '3')
                                                <div class="badge badge-warning">Panding</div>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('page-scripts')
<script type="text/javascript">
    $('#list_post').dataTable({
        "lengthChange": false,
        "ordering": false,
        "columnDefs": [{
            "className": "dt-center",
            "targets": "_all"
        }]
    });
</script>
@endpush