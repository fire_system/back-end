<div class="form-group">
    <label class="control-label">Title</label>
    <input type="text" class="form-control @error('txttitle') is-invalid @enderror" name="txttitle" value="{{ old('txttitle') ?? $news->title }}">
    @error('txttitle')
    	<div class="invalid-feedback">Title tidak boleh kosong !</div>
    @enderror
</div>
<div class="form-group">
    <label class="control-label">Thumbnail</label>
    <div class="custom-file">
        <input type="file" class="custom-file-input @error('txtthumbnail') is-invalid @enderror" name="txtthumbnail" value="{{ old('txtthumbnail') ?? $news->thumbnail }}">
        <label class="custom-file-label">Drop file here to upload</label>
    </div>
    @error('txtthumbnail')
    	<div class="invalid-feedback">Thumbnail tidak boleh kosong !</div>
    @enderror
</div>
<div class="form-group">
    <div class="ml-2 col-sm-6">
    @if (isset($news->thumbnail))
        <img class="img-fluid" src={{asset('storage/thumbnail/'.$news->thumbnail)}}>
    @else
        <img src="https://placehold.it/80x80" id="preview" class="img-thumbnail">
    @endif
    </div>
</div>
<div class="form-group">
    <label class="control-label">Category</label>
    <select class="form-control selectric @error('txtkategori') is-invalid @enderror" name="txtkategori">
        <option value="">-- select categori --</option>
        @foreach ($categories as $category)
            @if (old('txtkategori') == $category->id)
                <option selected value="{{ $category->id }}">{{ $category->name }}</option>
            @else
                <option {{ $category->id == $news->categori_id  ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
            @endif
        @endforeach
    </select>
    @error('txtkategori')
    <div class="invalid-feedback">Harap pilih kategori!</div>
    @enderror
</div>
<div class="form-group">
    <label class="control-label">Content</label>
    <textarea class="form-control @error('txtkonten') is-invalid @enderror" id="body_content" name="txtkonten" rows="40" cols="80">{{ old('txtkonten') ?? $news->content }}</textarea>
    @error('txtkonten')
    	<div class="invalid-feedback">Konten tidak boleh kosong !</div>
    @enderror
</div>
<div class="form-group">
    <label class="control-label">Tags</label>
    <input type="text" class="form-control inputtags @error('txttags') is-invalid @enderror" name="txttags" value="{{ old('txttags') ?? $news->tags }}">
    @error('txttags')
    <div class="invalid-feedback">Tag tidak boleh kosong !</div>
    @enderror
</div>
<div class="form-group">
    <label class="control-label">Author</label>
    <input type="text" class="form-control @error('txtauthor') is-invalid @enderror" name="txtauthor" value="{{ old('txtauthor') ?? $news->author }}">
    @error('txtauthor')
        <div class="invalid-feedback">Author tidak boleh kosong !</div>
    @enderror
</div>
<div class="form-group">
    <label class="control-label">Status</label>
    <select class="form-control selectric @error('txtstatus') is-invalid @enderror" name="txtstatus">
        <option value="">-- select status --</option>
        @foreach ($status as $stat)
            @if (old('txtstatus') == $stat->id)
                <option selected value="{{ $stat->id }}">{{ $stat->name }}</option>
            @else
                <option {{ $stat->id == $news->status ? 'selected' : '' }} value="{{ $stat->id }}">{{ $stat->name }}</option>
            @endif
        @endforeach
    </select>
    @error('txtstatus')
    <div class="invalid-feedback">Harap pilih status!</div>
    @enderror
</div>
<div class="form-group text-right">
	<button class="btn btn-primary">{{ $submit ?? 'Update Post' }}</button>
</div>