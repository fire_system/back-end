@extends('layouts.master')
@section('title', 'Update Post')
@section('content')
	<section class="section">
		<div class="section-header">
			<h1>Post</h1>
			<div class="section-header-breadcrumb">
				<div class="breadcrumb-item">Post</div>
				<div class="breadcrumb-item">Edit Entrie</div>
			</div>
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-12 col-md-6 col-lg-12">
					<div class="card">
						<div class="card-header">
							<h4>Edit Post</h4>
							<div class="card-header-form">
								<a href="{{ URL::previous() }}" class="btn btn-md btn-info"><i class="fas fa-arrow-left"></i> Back</a>
							</div>
						</div>
						<div class="card-body">
							<form action="/news/{{ $news->slug }}/edit" method="post" enctype="multipart/form-data">
								@method('patch')
								@csrf
								@include('pages.news.partials.form-control')
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('page-scripts')
<script>
  $("select").selectric();
	$(".inputtags").tagsinput('items');

  jQuery(function ($) {
  	$(document).ready(function(){
        $(".custom-file-input").on("change", function() {
          var fileName = $(this).val().split("\\").pop();
          $(this).siblings(".custom-file-label").addClass("selected").html(fileName);

          var reader = new FileReader();
          reader.onload = function(e){
            document.getElementById("preview").src = e.target.result;
          }

          reader.readAsDataURL(this.files[0]);
        });
    });
    CKEDITOR.replace('body_content');
  });
</script>
@endpush