<div class="form-group">
	<label class="control-label">Category Name</label>
	<input type="text" class="form-control  @error('category_name') is-invalid @enderror" name="category_name" value="{{ old('category_name') ?? $category->name }}">
	@error('category_name')
    	<div class="invalid-feedback">Nama Kategori tidak boleh kosong !</div>
  	@enderror
</div>
<div class="form-group text-right">
	<button class="btn btn-primary">{{ $submit ?? 'Update Category' }}</button>
</div>