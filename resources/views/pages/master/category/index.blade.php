@extends('layouts.master')
@section('title','Category')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>Category</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Category</a></div>
            <div class="breadcrumb-item">List Category</div>
        </div>
    </div>
    <div class="section-body">
        @if(session('success'))
        <div class="box-body">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p style="margin-bottom: 0;"><i class="icon fa fa-check"></i></i> {{ session('success') }}</p>
            </div>
        </div>
        @endif
        @if (count($errors) > 0)
        <div class="box-body">
            <div class="alert alert-danger alert-dismissible">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-12 col-md-6 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List Category</h4>
                        <div class="card-header-form">
                            <a href="{{ route('category.create') }}" class="btn btn-md btn-info" id="modal_menu_level_1"><i class="fas fa-plus"></i> Add Category</a>
                        </div>
                    </div>
                    <div class="card-body">
    					<div class="table-responsive">
    						<table id="list_post" class="table table-striped">
    							<thead>
    								<tr>
    									<th class="text-center pt-2">No</th>
    									<th>Category Name</th>
    									<th>Category Slug</th>
    								</tr>
    							</thead>
                                <tbody>
                                    @php $no = 1; @endphp
                                    @foreach ($category as $categories)
                                    <tr>
                                        <td class="text-center">{{ $no++ }}</td>
                                        <td>
                                            {{ $categories->name }}
                                            <div class="table-links">
                                                <form action="{{ route('category.destroy', [ 'category' => $categories->slug ]) }}" method="post" id="form">
                                                @method('delete')
                                                @csrf
                                                    <a href="{{ route('category.edit', [ 'category' => $categories->slug ]) }}" class="text-primary" title="Edit Category"><i class="fas fa-pencil-alt"></i></a>
                                                    <div class="bullet"></div>
                                                    <button type="submit" class="btn btn-link text-danger p-0" id="btn_delete"><i class="fas fa-trash-alt"></i></button>
												</form>
                                            </div>
                                        </td>
                                        <td>
                                            {{ $categories->slug }}
                                        </td>
									</tr>
									@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('page-scripts')

@endpush