@extends('layouts.master')
@section('title', 'Edit Category')
@section('content')
	<section class="section">
		<div class="section-header">
			<h1>Category</h1>
			<div class="section-header-breadcrumb">
				<div class="breadcrumb-item active"><a href="#">Category</a></div>
				<div class="breadcrumb-item">Edit Category</div>
			</div>
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-12 col-md-6 col-lg-12">
					<div class="card">
						<div class="card-header">
							<h4>Edit Category</h4>
							<div class="card-header-form">
								<a href="{{ URL::previous() }}" class="btn btn-md btn-info"><i class="fas fa-arrow-left"></i> Back</a>
							</div>
						</div>
						<div class="card-body">
							<form action="{{ route('category.update', [ 'category' => $category->slug ]) }}" method="post">
								@method('patch')
								@csrf
								@include('pages.master.category.partials.form-control')
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection