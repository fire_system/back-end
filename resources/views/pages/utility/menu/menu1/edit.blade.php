@extends('layouts.master')
@section('title','Edit Menu Level 1')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>Utility</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Utility</a></div>
            <div class="breadcrumb-item">Menu Level 1</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Menu Level 1</h4>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('utility.updateMn1', $menu1->id)}}">
                            @csrf
                            <div class="form-group">
                                <label>Menu Name</label>
                                <input type="text" class="form-control  @error('txtMenuName') is-invalid @enderror" placeholder="Menu Name" name="txtMenuName" value="{{ old('txtMenuName') ?? $menu1->menu_name }}">
                            </div>
                            <div class="form-group">
                                <label>Icon</label>
                                <input type="text" class="form-control  @error('txtMenuClass') is-invalid @enderror" placeholder="Menu Icon" name="txtMenuClass" value="{{ old('txtMenuClass') ?? $menu1->menu_class }}">
                            </div>
                            <div class="form-group">
                                <label>Link</label>
                                <input type="text" class="form-control  @error('txtMenuUrl') is-invalid @enderror" placeholder="Link" name="txtMenuUrl" value="{{ old('txtMenuUrl') ?? $menu1->menu_url }}">
                            </div>
                            <div class="form-group">
                                <label>Have Sub Menu</label>
                                <select class="form-control selectric @error('txtHaveSubMenu') is-invalid @enderror" name="txtHaveSubMenu">
                                    <option value="">-- select have sub menu --</option>
                                    <option {{ $menu1->have_submenu == '1' ? 'selected' : '' }} value="1">Have</option>
                                    <option {{ $menu1->have_submenu == '0' ? 'selected' : '' }} value="0">Not Have</option>
                                </select>
                            </div>
                            <div class="form-group float-right">
                                <button class="btn btn-md btn-primary"><i class="fas fa-save"></i> Simpan</button>
                                <button class="btn btn-md btn-danger" type="reset">Reset</button>
                                <a href="{{ URL::previous() }}" class="btn btn-md btn-info"><i class="fas fa-arrow-left"></i> Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('page-scripts')
<script>
    $("select").selectric();
</script>
@endpush