@extends('layouts.master')
@section('title','Edit Menu Level 2')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>Utility</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Utility</a></div>
            <div class="breadcrumb-item">Menu Level 2</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Menu Level 2</h4>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('utility.updateMn2', $menu2->id)}}">
                            @csrf
                            <div class="form-group">
                                <label>Main Menu</label>
                                <select class="form-control selectric @error('txtmainmenu') is-invalid @enderror" name="txtmainmenu">
                                    <option value="">-- select main menu --</option>
                                    @foreach($menu1 as $mn1)
                                        @if(old('txtmainmenu') == $mn1->id)
                                        <option select value="{{ $mn1->id }}">{{ $mn1->menu_name }}</option>
                                        @else
                                        <option {{ $mn1->id == $menu2->menu_id  ? 'selected' : '' }} value="{{ $mn1->id }}">{{ $mn1->menu_name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Sub Menu</label>
                                <input type="text" class="form-control  @error('txtSubMenu') is-invalid @enderror" placeholder="Sub Menu" name="txtSubMenu" value="{{ old('txtSubMenu') ?? $menu2->submenu_name }}">
                            </div>
                            <div class="form-group">
                                <label>Icon</label>
                                <input type="text" class="form-control  @error('txtIcon') is-invalid @enderror" placeholder="Icon" name="txtIcon" disabled>
                            </div>
                            <div class="form-group">
                                <label>Link</label>
                                <input type="text" class="form-control  @error('txtLink') is-invalid @enderror" placeholder="Link" name="txtLink" value="{{ old('txtLink') ?? $menu2->submenu_url }}">
                            </div>
                            <div class="form-group float-right">
                                <button class="btn btn-md btn-primary">Simpan</button>
                                <button class="btn btn-md btn-danger" type="reset">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('page-scripts')
<script>
    $("select").selectric();
</script>
@endpush