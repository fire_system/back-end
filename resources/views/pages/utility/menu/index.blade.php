@extends('layouts.master')
@section('title','Utility')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>Utility</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Utility</a></div>
            <div class="breadcrumb-item">List Menu</div>
        </div>
    </div>
    <div class="section-body">
        @if(session('success'))
        <div class="box-body">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p style="margin-bottom: 0;"><i class="icon fa fa-check"></i></i> {{ session('success') }}</p>
            </div>
        </div>
        @endif
        @if (count($errors) > 0)
        <div class="box-body">
            <div class="alert alert-danger alert-dismissible">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-12 col-md-6 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Menu Level 1</h4>
                        <div class="card-header-form">
                            <a href="javascript;:" class="btn btn-md btn-info" id="modal_menu_level_1"><i class="fas fa-plus"></i> Tambah Menu</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="list_menu1" class="table table-striped">
                                <thead>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Icon</th>
                                    <th>Url</th>
                                    <th>Sub Menu</th>
                                    <th>Opsi</th>
                                </thead>
                                <tbody>
                                    @php $no = 1; @endphp
                                    @foreach($menu1 as $lvl1)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $lvl1->menu_name }}</td>
                                        <td><i class="{{ $lvl1->menu_class }}"></i></td>
                                        <td>{{ $lvl1->menu_url }}</td>
                                        <td>
                                            @if($lvl1->have_submenu == '1')
                                            <div class="badge badge-success">Have</div>
                                            @else
                                            <div class="badge badge-danger">Not Have</div>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('utility.editMn1', $lvl1->id) }}" class="text-primary" title="Edit Article"><i class="fas fa-pencil-alt"></i></a>
                                            <div class="bullet"></div>
                                            <a href="{{ route('utility.deleteMn1', $lvl1->id) }}" class="text-danger" title="Edit Article"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4>Menu Level 2</h4>
                        <div class="card-header-form">
                            <a href="javascript" class="btn btn-md btn-info" id="modal_menu_level_2"><i class="fas fa-plus"></i> Tambah Menu</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="list_menu2" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Menu</th>
                                        <th>Sub Menu</th>
                                        <th>Url</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $no = 1; @endphp
                                    @foreach($menu2 as $lvl2)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>
                                            @foreach($menu1 as $lvl1)
                                            @if( $lvl2->menu_id === $lvl1->id )
                                                {{ $lvl1->menu_name }}
                                            @endif
                                            @endforeach
                                        </td>
                                        <td>{{ $lvl2->submenu_name }}</td>
                                        <td>{{ $lvl2->submenu_url }}</td>
                                        <td>
                                            <a href="{{ route('utility.editMn2', $lvl2->id) }}" class="text-primary" title="Edit Article"><i class="fas fa-pencil-alt"></i></a>
                                            <div class="bullet"></div>
                                            <a href="{{ route('utility.deleteMn2', $lvl2->id) }}" class="text-danger" title="Edit Article"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<form method="post" action="{{route('utility.saveMn1')}}" class="modal-part" id="modal_mn_lvl_1">
    @csrf
    <div class="form-group">
        <label>Nama Menu</label>
        <input type="text" class="form-control" placeholder="Nama Menu" name="txtMenuName">
    </div>
    <div class="form-group">
        <label>Icon</label>
        <input type="text" class="form-control" placeholder="Icon" name="txtMenuClass">
    </div>
    <div class="form-group">
        <label>Url</label>
        <input type="text" class="form-control" placeholder="Url" name="txtMenuUrl">
    </div>
    <div class="form-group">
        <label>Have Sub Menu</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="txtHaveSubMenu" id="status1" value="1" checked>
            <label class="form-check-label" for="status1">
                True
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="txtHaveSubMenu" id="status2" value="0">
            <label class="form-check-label" for="status2">
                False
            </label>
        </div>
    </div>
    <div class="form-group float-right">
        <button class="btn btn-md btn-primary">Simpan</button>
        <button class="btn btn-md btn-danger" type="reset">Reset</button>
    </div>
</form>
<form method="post" action="{{route('utility.saveMn2')}}" class="modal-part" id="modal_mn_lvl_2">
    @csrf
    <div class="form-group">
        <label>Main Menu</label>
        <select class="form-control" name="txtmainmenu">
            <option value="">-- select main menu --</option>
            @foreach($menu1 as $mnlvl1)
            <option value="{{ $mnlvl1->id }}">{{ $mnlvl1->menu_name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Sub Menu</label>
        <input type="text" class="form-control" placeholder="Sub Menu" name="txtSubMenu">
    </div>
    <div class="form-group">
        <label>Icon</label>
        <input type="text" class="form-control" placeholder="Icon" name="txtIcon" disabled>
    </div>
    <div class="form-group">
        <label>Link</label>
        <input type="text" class="form-control" placeholder="Link" name="txtLink">
    </div>
    <div class="form-group float-right">
        <button class="btn btn-md btn-primary">Simpan</button>
        <button class="btn btn-md btn-danger" type="reset">Reset</button>
    </div>
</form>
@endsection

@push('page-scripts')
<script>
    jQuery(function($) {
        $('#list_menu1, #list_menu2').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : false,
            'info'        : true,
            'autoWidth'   : true
        });
        $("#modal_menu_level_1").fireModal({
            title: 'Menu Level 1',
            body: $("#modal_mn_lvl_1")
        });
        $("#modal_menu_level_2").fireModal({
            title: 'Menu Level 2',
            body: $("#modal_mn_lvl_2")
        });

    });
</script>
@endpush