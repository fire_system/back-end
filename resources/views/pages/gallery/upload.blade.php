@extends('layouts.master')
@section('title', 'Our Gallery')
@section('content')
	<section class="section">
		<div class="section-header">
			<h1>Our Gallery</h1>
			<div class="section-header-breadcrumb">
				<div class="breadcrumb-item active"><a href="{{ route('assets.gallery') }}">Our Gallery</a></div>
			</div>
		</div>
		<div class="section-body">
			@if(session('success'))
	      <div class="box-body">
	          <div class="alert alert-success alert-dismissible">
	              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	              <p style="margin-bottom: 0;"><i class="icon fa fa-check"></i></i> {{ session('success') }}</p>
	          </div>
	      </div>
      @endif
      @if (count($errors) > 0)
	      <div class="box-body">
	          <div class="alert alert-danger alert-dismissible">
	              <ul>
	                  @foreach ($errors->all() as $error)
	                      <li>{{ $error }}</li>
	                  @endforeach
	              </ul>
	          </div>
	      </div>
      @endif
			<div class="row">
				<div class="col-12 col-md-6 col-lg-12">
					<div class="card">
            <div class="card-header">
                <h4>Upload New Gallery</h4>
            </div>
						<div class="card-body">
							<form method="post" action="{{route('assets.gallery.store')}}" enctype="multipart/form-data" class="dropzone" id="image-upload">
						    @csrf
							</form>
							<div align="center" class="mt-3">
						    <button type="button" class="btn btn-info" id="submit-all">Upload</button>
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@push('page-scripts')
	<script type="text/javascript">
		"use strict";

		Dropzone.autoDiscover = false;

		var dropzn = new Dropzone('#image-upload', {
			autoProcessQueue: false,
			dictDefaultMessage: 'Drop Photo Gallery or click here to upload',
			maxFiles: 10,
			maxFilesize: 1,
			addRemoveLinks: true,
			acceptedFiles: '.jpg,.jpeg,.png',
			uploadMultiple: true,
			renameFile: function (file) {
        var dt = new Date();
        var time = dt.getTime();
        return time + file.name;
      },
      timeout: 60000,
      init: function(){
				var myDropzone = this;

				$('#submit-all').on('click', function(e){
					if (myDropzone.files.length > 0) {
						e.preventDefault();
						myDropzone.processQueue();
						myDropzone.on('completemultiple', function(file){
							swal({
								title: 'Upload Success..!',
								text: 'Upload Images are succes',
								icon: 'success'
							}).then(function(clickable){
								if (clickable) {
									window.location = "{{ route('assets.gallery') }}";
								}
							});
							myDropzone.removeFile(file);
						});
					} else {
						myDropzone.uploadFiles([]);
					}
				});
			}
		});
	</script>
@endpush