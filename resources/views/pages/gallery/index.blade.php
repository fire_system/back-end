@extends('layouts.master')
@section('title', 'Our Gallery')
@section('content')
<section class="section">
    <div class="section-header">
        <h1>Our Gallery</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('assets.gallery') }}">Our Gallery</a></div>
        </div>
    </div>
    <div class="section-body">
        @if(session('success'))
        <div class="box-body">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p style="margin-bottom: 0;"><i class="icon fa fa-check"></i></i> {{ session('success') }}</p>
            </div>
        </div>
        @endif
        @if (count($errors) > 0)
        <div class="box-body">
            <div class="alert alert-danger alert-dismissible">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-12 col-md-6 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List Gallery</h4>
                        <div class="card-header-form">
                            <a href="{{ route('assets.gallery.upload') }}" class="btn btn-md btn-info"><i
                                    class="fas fa-plus"></i> Tambah Foto</a>
                        </div>
                    </div>
                    <div class="card-body gallery">
						<div class="row">
							@foreach ($dataimage as $element)
								<div class="col-sm-2 p-2">
									<div class="box-gallery">
										<img src="{{ asset('storage/gallery/'.$element->image) }}" class="img-fluid" data-image="{{ asset('storage/gallery/'.$element->image) }}" data-title="{{ $element->title }}" title="{{ $element->title }}" style="background-image: url('{{ asset('storage/gallery/'.$element->image) }}'); margin: 10px;">
										<div class="box-icon">
											<button class="btn btn-sm btn-icon btn-danger" id="{{ $element->title }}">
												<i class="fas fa-trash-alt"></i>
											</button>
										</div>
									</div>
								</div>
							@endforeach
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('page-scripts')
<script type="text/javascript">
    $('#table-image').dataTable({
        "lengthChange": false,
        "ordering": false,
        "columnDefs": [{
            "className": "dt-center",
            "targets": "_all"
        }]
    });

    $('#table-image tbody').on('click', 'tr', function () {
        let $this = $(this);
        let currentRow = $this.closest('tr');

        let title = currentRow.find('td:eq(1)').text();

        swal({
                title: 'Are you sure?',
                text: 'Once deleted, you will not be able to recover this imaginary file!',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.post("{{ route('assets.gallery.destroy') }}", {
                        'title': title,
                        '_token': {{ csrf_token() }}
                    }, function (result) {
                        if (result) {
                            swal('Poof! Your imaginary file has been deleted!', {
                                icon: 'success',
                            }).then((clickable) => {
                                if (clickable) {
                                    window.location = "{{ route('assets.gallery') }}";
                                }
                            });
                        } else {
                            swal('Poof! Your imaginary file failed to deleted!', {
                                icon: 'error',
                            });
                        }
                    });
                } else {
                    swal('Your imaginary file is safe!');
                }
            });
    });
</script>
@endpush