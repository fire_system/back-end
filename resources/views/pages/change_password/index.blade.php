@extends('layouts.master')
@section('title', 'Change Password')
@section('content')
	<section class="section">
		<div class="section-header">
			<h1>Change Password</h1>
			<div class="section-header-breadcrumb">
				<div class="breadcrumb-item active"><a href="{{ route('change_pass') }}">Change Password</a></div>
			</div>
		</div>
		<div class="section-body">
			@if(session('success'))
	      <div class="box-body">
	          <div class="alert alert-success alert-dismissible">
	              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	              <p style="margin-bottom: 0;"><i class="icon fa fa-check"></i></i> {{ session('success') }}</p>
	          </div>
	      </div>
      @endif
      @if (count($errors) > 0)
	      <div class="box-body">
	          <div class="alert alert-danger alert-dismissible">
	              <ul>
	                  @foreach ($errors->all() as $error)
	                      <li>{{ $error }}</li>
	                  @endforeach
	              </ul>
	          </div>
	      </div>
      @endif
			<div class="row">
				<div class="col-12 col-md-6 col-lg-12">
					<div class="card">
						<div class="card-body">
							<form action="{{ route('change.password') }}" method="post">
								@csrf
								<div class="form-group col-4">
									<label class="control-label">Current Password</label>
									<input type="password" class="form-control  @error('current_password') is-invalid @enderror" name="current_password" value="{{ old('current_password') ?? '' }}">
									@error('current_password')
								    	<div class="invalid-feedback">Current Password can't be empty !</div>
								  	@enderror
								</div>
								<div class="form-group col-4">
									<label class="control-label">New Password</label>
									<input type="password" class="form-control  @error('new_password') is-invalid @enderror" name="new_password" value="{{ old('new_password') ?? '' }}">
									@error('new_password')
								    	<div class="invalid-feedback">New Password can't be empty !</div>
								  	@enderror
								</div>
								<div class="form-group col-4">
									<label class="control-label">Confirm Password</label>
									<input type="password" class="form-control  @error('new_confirm_password') is-invalid @enderror" name="new_confirm_password" value="{{ old('new_confirm_password') ?? '' }}">
									@error('new_confirm_password')
								    	<div class="invalid-feedback">Confirm Password can't be empty !</div>
								  	@enderror
								</div>
								<div class="form-group text-right">
									<button class="btn btn-primary">Update</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection