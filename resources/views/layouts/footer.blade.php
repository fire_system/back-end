<footer class="main-footer">
    <div class="footer-left">
        PT Rasa Nday Suksindo &copy; Copyright {{ date('Y') }} - Powered by Imaginative
    </div>
    <div class="footer-right">
        0.0.1
    </div>
</footer>