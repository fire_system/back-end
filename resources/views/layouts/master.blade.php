<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>@yield('title') &mdash; Firesystem</title>
    <link rel="icon" href="{{ asset('img/favicon.png') }}" type = "image/x-icon">
    <link rel="apple-touch-icon" href="{{ asset('img/favicon.png') }}" type = "image/x-icon">
    <link rel="stylesheet" href="{{ asset('plugin/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugin/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugin/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugin/selectric/public/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('plugin/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
    <link rel="stylesheet" href="{{ asset('plugin/chocolat/dist/css/chocolat.css') }}">
    <link rel="stylesheet" href="{{ asset('plugin/dropzone/dist/dropzone.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/components.css') }}">
</head>

<body>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>
            @include('layouts.navbar')
            @include('layouts.sidebar')
            <div class="main-content">
                @yield('content')
            </div>
            @include('layouts.footer')
        </div>
    </div>

    @stack('before-scripts')
    <script src="{{ asset('plugin/jquery/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('plugin/popper/popper.min.js') }}"></script>
    <script src="{{ asset('plugin/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugin/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('plugin/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ asset('plugin/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('plugin/moment/moment.min.js') }}"></script>
    <script src="{{ asset('plugin/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('plugin/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ asset('plugin/selectric/public/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('plugin/uploadpreview/jquery.uploadPreview.min.js') }}"></script>
    <script src="{{ asset('plugin/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('plugin/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>
    <script src="{{ asset('plugin/dropzone/dist/dropzone.js') }}"></script>
    <script src="{{ asset('js/stisla.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/bootbox.js') }}"></script>
    @stack('page-scripts')
</body>

</html>
