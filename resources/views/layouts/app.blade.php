<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Firesystem</title>
    <link rel="icon" href="{{ asset('img/favicon.png') }}" type = "image/x-icon">
    <link rel="apple-touch-icon" href="{{ asset('img/favicon.png') }}" type = "image/x-icon">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- General JS Scripts -->
    <script src="{{URL::asset('plugin/jquery/jquery-3.4.1.min.js')}}"></script>
    <script src="{{URL::asset('plugin/popper/popper.min.js')}}"></script>
    <script src="{{URL::asset('plugin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('plugin/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <script src="{{URL::asset('plugin/moment/moment.min.js')}}"></script>
    <script src="{{URL::asset('js/stisla.js')}}"></script>

    <!-- Template JS File -->
    <script src="{{URL::asset('js/scripts.js')}}"></script>
    <script src="{{URL::asset('js/custom.js')}}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('plugin/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/components.css')}}">
</head>
<body>
    <div id="app">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
