<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('home') }}">Firesystem</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('home') }}">CM</a>
        </div>
        <ul class="sidebar-menu">
            <li class="nav-item active">
                <a href="{{ route('home') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            @foreach (MenuHelpers::menu_level1() as $menulevel1)
                @if ($menulevel1->have_submenu == '1')
                    <li class="nav-item dropdown">
                        <a href="javascript:;" class="nav-link has-dropdown" data-toggle="dropdown"><i class="{{ $menulevel1->menu_class }}"></i> <span>{{ $menulevel1->menu_name }}</span></a>
                        @foreach (MenuHelpers::menu_level2() as $menulevel2)
                            @if ($menulevel1->id == $menulevel2->menu_id)
                                <ul class="dropdown-menu">
                                    <li>
                                        <a class="nav-link" href="{{ route($menulevel2->submenu_url) }}">{{ $menulevel2->submenu_name }}
                                        @if ($menulevel2->submenu_name == 'Published')
                                            ({{ MenuHelpers::countPost('1') }})
                                        @elseif ($menulevel2->submenu_name == 'Draft')
                                            ({{ MenuHelpers::countPost('2') }})
                                        @elseif ($menulevel2->submenu_name == 'Pending')
                                            ({{ MenuHelpers::countPost('3') }})
                                        @elseif ($menulevel2->submenu_name == 'All')
                                            ({{ MenuHelpers::countPost('all') }})
                                        @endif</a>
                                    </li>
                                </ul>
                            @endif
                        @endforeach
                    </li>
                @else
                    <li class="nav-item">
                        <a href="{{ route($menulevel1->menu_url) }}" class="nav-link"><i class="{{ $menulevel1->menu_class }}"></i><span>{{ $menulevel1->menu_name }}</span></a>
                    </li>
                @endif
            @endforeach
        </ul>
    </aside>
</div>
